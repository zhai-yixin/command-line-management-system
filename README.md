# 命令行界面-企业员工管理系统

#### 登录
![登录](https://foruda.gitee.com/images/1706070211944236068/c1ba7c0f_13329058.png "屏幕截图")

#### 人员信息
![增删改查人员信息](https://foruda.gitee.com/images/1706069978755408836/e216b58c_13329058.png "屏幕截图")

#### 财务信息
![增删改查财务信息](https://foruda.gitee.com/images/1706070271076767579/a052dfe7_13329058.png "屏幕截图")

#### 请假信息
![增删改查请假信息](https://foruda.gitee.com/images/1706070305920621010/3263d18c_13329058.png "屏幕截图")

#### 数据库的ER图：
![ER图](https://foruda.gitee.com/images/1706070050638343684/9224a3bf_13329058.png "屏幕截图")