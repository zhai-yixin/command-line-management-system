/*==============================================================*/
/* DBMS name:      PostgreSQL 9.x                               */
/* Created on:     2023/5/1 11:00:08                            */
/*==============================================================*/


drop table department;

drop table income_expense_record;

drop table recess_record;

drop table staff_member;

/*==============================================================*/
/* Table: department                                            */
/*==============================================================*/
create table department (
   dept_name            VARCHAR(30)          not null,
   location             VARCHAR(30)          not null,
   constraint PK_DEPARTMENT primary key (dept_name)
);

/*==============================================================*/
/* Table: income_expense_record                                 */
/*==============================================================*/
create table income_expense_record (
   dept_name            VARCHAR(30)          not null,
   income_expense_time  DATE                 not null,
   income_expense       FLOAT8               not null,
   constraint PK_INCOME_EXPENSE_RECORD primary key (dept_name, income_expense_time)
);

/*==============================================================*/
/* Table: recess_record                                         */
/*==============================================================*/
create table recess_record (
   ID_card_number       VARCHAR(30)          not null,
   start_time           DATE                 not null,
   end_time             DATE                 not null,
   checked              BOOL                 not null,
   reason               VARCHAR(30)          null,
   admitted             BOOL                 null,
   constraint PK_RECESS_RECORD primary key (ID_card_number, start_time)
);

/*==============================================================*/
/* Table: staff_member                                          */
/*==============================================================*/
create table staff_member (
   ID_card_number       VARCHAR(30)          not null,
   dept_name            VARCHAR(30)          not null,
   supervisor_ID        VARCHAR(30)          null,
   name                 VARCHAR(30)          not null,
   gender               CHAR(1)              not null,
   birth_date           DATE                 not null,
   joining_data         DATE                 not null,
   duty                 VARCHAR(30)          not null,
   salary               FLOAT8                not null,
   password             VARCHAR(30)          not null,
   administrator        BOOL                 not null,
   residence_address    VARCHAR(30)          null,
   phone_number         VARCHAR(30)          null,
   email_address        VARCHAR(30)          null,
   constraint PK_STAFF_MEMBER primary key (ID_card_number)
);

alter table income_expense_record
   add constraint FK_INCOME_E_FINANCE_DEPARTME foreign key (dept_name)
      references department (dept_name)
      on delete cascade on update cascade;

alter table recess_record
   add constraint FK_RECESS_R_RECESS_STAFF_ME foreign key (ID_card_number)
      references staff_member (ID_card_number)
      on delete cascade on update cascade;

alter table staff_member
   add constraint FK_STAFF_ME_BELONG_DEPARTME foreign key (dept_name)
      references department (dept_name)
      on delete restrict on update cascade;

alter table staff_member
   add constraint FK_STAFF_ME_SUPERVISO_STAFF_ME foreign key (supervisor_ID)
      references staff_member (ID_card_number)
      on delete restrict on update cascade;
